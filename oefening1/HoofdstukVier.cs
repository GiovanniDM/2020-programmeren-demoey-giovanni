﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Programmeren
{
    public class HoofdstukVier
    {
        enum Hoofdstuk4 { BMIberekenaar = 1, Pythagoras, Cirkels, Orakeltje }
        static public void switchVier()
        {

            Console.WriteLine("Welke oefening wilt u openen?");
            Console.WriteLine($"{(int)Hoofdstuk4.BMIberekenaar}. {Hoofdstuk4.BMIberekenaar}");
            Console.WriteLine($"{(int)Hoofdstuk4.Pythagoras}. {Hoofdstuk4.Pythagoras}");
            Console.WriteLine($"{(int)Hoofdstuk4.Cirkels}. {Hoofdstuk4.Cirkels}");
            Console.WriteLine($"{(int)Hoofdstuk4.Orakeltje}. {Hoofdstuk4.Orakeltje}");
            Hoofdstuk4 Keuze = (Hoofdstuk4)Convert.ToInt32(Console.ReadLine());
            switch (Keuze)
            {
                case Hoofdstuk4.BMIberekenaar:
                    Console.Clear();
                    BMIBerekenaar();
                    break;
                case Hoofdstuk4.Pythagoras:
                    Console.Clear();
                    Pythagoras();
                    break;
                case Hoofdstuk4.Cirkels:
                    Console.Clear();
                    Cirkels();
                    break;
                case Hoofdstuk4.Orakeltje:
                    Console.Clear();
                    Orakeltje();
                    break;
            }
        }

        static public void BMIBerekenaar()
        {
            Console.WriteLine("Hoeveel weeg je in kg?");
            double weight = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Hoe groot ben je in m?");
            double height = Convert.ToDouble(Console.ReadLine());
            double heightforumule = Math.Pow(height, 2);
            Console.WriteLine($"Je BMI bedraagt {weight/heightforumule:F2}.");
        }

        static public void Pythagoras()
        {
            Console.WriteLine("Geef de lengte van de eerste rechterhoekszijde.");
            int firstside = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Geef de lengte van de tweede rechterhoekszijde.");
            int secondside = Convert.ToInt32(Console.ReadLine());
            int thirdside = Convert.ToInt32(Math.Pow(firstside, 2) + Math.Pow(secondside, 2));
            Console.Write($"De lengte van de schuine zijde is {Math.Sqrt(thirdside)}");
        }

        static public void Cirkels()
        {
            Console.WriteLine("Geef de straal");
            double straal = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine($"De omtrek van een circkel met straal {straal} is {2 * straal * Math.PI:F2}");
            Console.WriteLine($"De oppervlakte is {Math.Pow(straal, 2) * Math.PI:F2}");
        }

        static public void Orakeltje()
        {
            Random randomgen = new Random();
            int ageadder = randomgen.Next(20,125);
            Console.WriteLine("Hoe oud ben je nu?");
            int age = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Je zal nog {ageadder} jaar leven. Je zal dus {age+ageadder} worden.");
        }
    }
}
