﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Programmeren
{
    class APDotCom
    {
        enum APDotComEnum {Bestel=1, BestelMetVraagEnAanbod, BestelConditioneel}

        static public void switchAPDotCom()
        {
            Console.WriteLine("Welke oefening wilt u openen?");
            Console.WriteLine($"{(int)APDotComEnum.Bestel}. {APDotComEnum.Bestel}");
            Console.WriteLine($"{(int)APDotComEnum.BestelMetVraagEnAanbod}. {APDotComEnum.BestelMetVraagEnAanbod}");
            Console.WriteLine($"{(int)APDotComEnum.BestelConditioneel}. {APDotComEnum.BestelConditioneel}");
            APDotComEnum Keuze = (APDotComEnum)Convert.ToInt32(Console.ReadLine());
            switch (Keuze)
            {
                case APDotComEnum.Bestel:
                    Console.Clear();
                    Bestel();
                    break;
                case APDotComEnum.BestelMetVraagEnAanbod:
                    Console.Clear();
                    BestelMetVraagEnAanbod();
                    break;
                case APDotComEnum.BestelConditioneel:
                    Console.Clear();
                    BestelConditioneel();
                    break;
            }
        }

        static public void Bestel()
        {
            Console.WriteLine("Prijs van een boek?");
            float priceBook = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Prijs van een CD?");
            float priceCD = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Prijs van servies?");
            float priceServies = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Prijs van een springkasteel?");
            float priceBounceCastle = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal boeken?");
            int countBook = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal CD\'s?");
            int countCD = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal servies?");
            int countServies = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal springkastelen?");
            int countBounceCastle = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal korting?");
            int discount = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Uw kasticket\n--------------\nBoek x {countBook}: {priceBook * countBook}\nCD x {countCD}: {countCD * priceCD}\nServies x {countServies}: {countServies*priceServies}\nSpringkasteel x {countBounceCastle}: {countBounceCastle*priceBounceCastle}\nKORTING: {discount}%\nTOTAAL VOOR KORTING: {(countCD*priceCD)+(countBook*priceBook)+(countBounceCastle*priceBounceCastle)+(countServies*priceServies)}\nTOTAAL: {((countCD * priceCD) + (countBook * priceBook) + (countBounceCastle * priceBounceCastle) + (countServies * priceServies))/(1-(discount/100))}");
        }

        static public void BestelMetVraagEnAanbod()
        {
            Console.WriteLine("Basisprijs van een boek?");
            float priceBook = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Basisprijs van een CD?");
            float priceCD = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Basisprijs van servies?");
            float priceServies = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Basisprijs van een springkasteel?");
            float priceBounceCastle = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal boeken?");
            int countBook = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal CD\'s?");
            int countCD = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal servies?");
            int countServies = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal springkastelen?");
            int countBounceCastle = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal korting?");
            int discount = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Uw kasticket\n---------------");
            Random randomgen = new Random();
            int vraagAanbod1 = randomgen.Next(-50, 50);
            int vraagAanbod2 = randomgen.Next(-50, 50);
            int vraagAanbod3 = randomgen.Next(-50, 50);
            int vraagAanbod4 = randomgen.Next(-50, 50);
            Console.WriteLine($"Vraag en aanbod boeken: {vraagAanbod1}%\nVraag en aanbod CD's: {vraagAanbod2}%\nVraag en aanbod serviezen: {vraagAanbod3}%\nVraag en aanbod springkastelen: {vraagAanbod4}%");
            Console.WriteLine($"Boek x {countBook}: {(((priceBook / 100) * vraagAanbod1) + priceBook) * countBook}\nCD x {countCD}: {(((priceCD / 100) * vraagAanbod2) + priceCD) * countCD}\n Servies x {countServies}: {(((priceServies / 100) * vraagAanbod3) + priceServies)*countServies}\nSpringkasteel x {countBounceCastle}: {(((priceBounceCastle / 100) * vraagAanbod4) + priceBounceCastle) * countBounceCastle}");
            Console.WriteLine($"KORTING: {discount}\nTOTAAL VOOR KORTING: {((((priceBook / 100) * vraagAanbod1)+priceBook) * countBook) + ((((priceCD / 100) * vraagAanbod2)+priceCD) * countCD) + ((((priceServies / 100) * vraagAanbod3)+priceServies) * countServies) + ((((priceBounceCastle / 100) * vraagAanbod4)+priceBounceCastle) * countBounceCastle)}");
            Console.WriteLine($"TOTAAL: {(((((priceBook / 100) * vraagAanbod1)+priceBook) * countBook) + ((((priceCD / 100) * vraagAanbod2)+priceCD) * countCD) + ((((priceServies / 100) * vraagAanbod3)+priceServies) * countServies) + ((((priceBounceCastle / 100) * vraagAanbod4)+priceBounceCastle) * countBounceCastle))/ (1 - (discount / 100))}");
        }
        static public void BestelConditioneel()
        {
            Console.WriteLine("Basisprijs van een boek?");
            float priceBook = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Basisprijs van een CD?");
            float priceCD = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Basisprijs van servies?");
            float priceServies = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Basisprijs van een springkasteel?");
            float priceBounceCastle = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal boeken?");
            int countBook = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal CD\'s?");
            int countCD = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal servies?");
            int countServies = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal springkastelen?");
            int countBounceCastle = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal korting?");
            int discount = Convert.ToInt32(Console.ReadLine());
            if (discount > 30)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Waarschuwing: het ingevoerde percentage is hoog!");
                Console.ResetColor();
            }
            Console.WriteLine("Worden de prijsstijgingen en -dalingen toegepast? (J/N)");
            string answer = Console.ReadLine();
            if(answer == "J") {
                Console.WriteLine("Uw kasticket\n---------------");
                Random randomgen = new Random();
                int vraagAanbod1 = randomgen.Next(-50, 50);
                int vraagAanbod2 = randomgen.Next(-50, 50);
                int vraagAanbod3 = randomgen.Next(-50, 50);
                int vraagAanbod4 = randomgen.Next(-50, 50);
                Console.WriteLine($"Vraag en aanbod boeken: {vraagAanbod1}%\nVraag en aanbod CD's: {vraagAanbod2}%\nVraag en aanbod serviezen: {vraagAanbod3}%\nVraag en aanbod springkastelen: {vraagAanbod4}%");
                Console.WriteLine($"Boek x {countBook}: {(((priceBook / 100) * vraagAanbod1) + priceBook) * countBook}\nCD x {countCD}: {(((priceCD / 100) * vraagAanbod2) + priceCD) * countCD}\n Servies x {countServies}: {(((priceServies / 100) * vraagAanbod3) + priceServies) * countServies}\nSpringkasteel x {countBounceCastle}: {(((priceBounceCastle / 100) * vraagAanbod4) + priceBounceCastle) * countBounceCastle}");
                Console.WriteLine($"KORTING: {discount}\nTOTAAL VOOR KORTING: {((((priceBook / 100) * vraagAanbod1) + priceBook) * countBook) + ((((priceCD / 100) * vraagAanbod2) + priceCD) * countCD) + ((((priceServies / 100) * vraagAanbod3) + priceServies) * countServies) + ((((priceBounceCastle / 100) * vraagAanbod4) + priceBounceCastle) * countBounceCastle)}");
                Console.WriteLine($"TOTAAL: {(((((priceBook / 100) * vraagAanbod1) + priceBook) * countBook) + ((((priceCD / 100) * vraagAanbod2) + priceCD) * countCD) + ((((priceServies / 100) * vraagAanbod3) + priceServies) * countServies) + ((((priceBounceCastle / 100) * vraagAanbod4) + priceBounceCastle) * countBounceCastle)) / (1 - (discount / 100))}");
            }
            Console.WriteLine($"Uw kasticket\n--------------\nBoek x {countBook}: {priceBook * countBook}\nCD x {countCD}: {countCD * priceCD}\nServies x {countServies}: {countServies * priceServies}\nSpringkasteel x {countBounceCastle}: {countBounceCastle * priceBounceCastle}\nKORTING: {discount}%\nTOTAAL VOOR KORTING: {(countCD * priceCD) + (countBook * priceBook) + (countBounceCastle * priceBounceCastle) + (countServies * priceServies)}\nTOTAAL: {((countCD * priceCD) + (countBook * priceBook) + (countBounceCastle * priceBounceCastle) + (countServies * priceServies)) / (1 - (discount / 100))}");
        }
    }
}
