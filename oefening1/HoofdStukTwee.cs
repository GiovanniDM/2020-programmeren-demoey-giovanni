﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Programmeren
{
    public class HoofdStukTwee
    {
        enum Hoofdstuk2 {Optellen=1, VerbruikWagen, BeetjeWiskunde, Gemiddelde, Maaltafels, Ruimte}

        static public void switchTwee()
        {

            Console.WriteLine("Welke oefening wilt u openen?");
            Console.WriteLine($"{(int)Hoofdstuk2.Optellen}. {Hoofdstuk2.Optellen}");
            Console.WriteLine($"{(int)Hoofdstuk2.VerbruikWagen}. {Hoofdstuk2.VerbruikWagen}");
            Console.WriteLine($"{(int)Hoofdstuk2.BeetjeWiskunde}. {Hoofdstuk2.BeetjeWiskunde}");
            Console.WriteLine($"{(int)Hoofdstuk2.Gemiddelde}. {Hoofdstuk2.Gemiddelde}");
            Console.WriteLine($"{(int)Hoofdstuk2.Maaltafels}. {Hoofdstuk2.Maaltafels}");
            Console.WriteLine($"{(int)Hoofdstuk2.Ruimte}. {Hoofdstuk2.Ruimte}");
            Hoofdstuk2 Keuze = (Hoofdstuk2)Convert.ToInt32(Console.ReadLine());
            switch (Keuze)
            {
                case Hoofdstuk2.Optellen:
                    Console.Clear();
                    Optellen();
                    break;
                case Hoofdstuk2.VerbruikWagen:
                    Console.Clear();
                    VerbruikWagen();
                    break;
                case Hoofdstuk2.BeetjeWiskunde:
                    Console.Clear();
                    BeetjeWiskunde();
                    break;
                case Hoofdstuk2.Gemiddelde:
                    Console.Clear();
                    Gemiddelde();
                    break;
                case Hoofdstuk2.Maaltafels:
                    Console.Clear();
                    Maaltafels();
                    break;
                case Hoofdstuk2.Ruimte:
                    Console.Clear();
                    Ruimte();
                    break;
            }
        }

        static public void Optellen()
        {
            Console.WriteLine("Wat is het eerste getal?");
            int number1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Wat is het tweede getal?");
            int number2 = Convert.ToInt32(Console.ReadLine());
            int result = number1 + number2;
            Console.WriteLine($"De som is {result} .");
        }

        static public void VerbruikWagen()
        {
            Console.WriteLine("Geef het aantal liter in tank voor de rit:");
            double fuelBefore = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Geef het aantal liter in tank na de rit:");
            double fuelAfter = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Geef kilometerstand voor de rit:");
            double distanceBefore = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Geef kilometerstand na de rit:");
            double distanceAfter = Convert.ToInt32(Console.ReadLine());
            double result = (100 * (fuelBefore - fuelAfter) / (distanceAfter - distanceBefore));
            Console.WriteLine($"Het verbruik van de auto is: {result}");
        }

        static public void BeetjeWiskunde()
        {
            int result1 = -1 + 4 * 6;
            int result2 = (35 + 5) % 7;
            int result3 = 14 + -4 * 6 / 11;
            int result4 = 2 + 15 / 6 * 1 - 7 % 2;
            Console.WriteLine(result1);
            Console.WriteLine(result2);
            Console.WriteLine(result3);
            Console.WriteLine(result4);
        }

        static public void Gemiddelde()
        {
            int result = (18 + 11 + 8)/3;
            Console.WriteLine(result);
        }

        static public void Maaltafels()
        {
            int number = 411;
            Console.Clear();
            int result1 = number * 1;
            Console.WriteLine($"1 * {number} is {result1}");
            Console.ReadLine();
            Console.Clear();
            int result2 = number * 2;
            Console.WriteLine($"2 * {number} is {result2}");
            Console.ReadLine();
            Console.Clear();
            int result3 = number * 3;
            Console.WriteLine($"3 * {number} is {result3}");
            Console.ReadLine();
            Console.Clear();
            int result4 = number * 4;
            Console.WriteLine($"4 * {number} is {result4}");
            Console.ReadLine();
            Console.Clear();
            int result5 = number * 5;
            Console.WriteLine($"5 * {number} is {result5}");
            Console.ReadLine();
            Console.Clear();
            int result6 = number * 6;
            Console.WriteLine($"6 * {number} is {result6}");
            Console.ReadLine();
            Console.Clear();
            int result7 = number * 7;
            Console.WriteLine($"7 * {number} is {result7}");
            Console.ReadLine();
            Console.Clear();
            int result8 = number * 8;
            Console.WriteLine($"8 * {number} is {result8}");
            Console.ReadLine();
            Console.Clear();
            int result9 = number * 9;
            Console.WriteLine($"9 * {number} is {result9}");
            Console.ReadLine();
            Console.Clear();
            int result10 = number * 10;
            Console.WriteLine($"10 * {number} is {result10}");
            Console.ReadLine();
            Console.Clear();
        }

        static public void Ruimte()
        {
            double weight = 70;
            double zwMerc = 0.38;
            double zwVenu = 0.91;
            double zwAard = 1.00;
            double zwMars = 0.38;
            double zwJupi = 2.34;
            double zwSatu = 1.06;
            double zwUran = 0.92;
            double zwNept = 1.19;
            double zwPlut = 0.06;

            Console.WriteLine($"Op Mercurius voel je je alsof je {zwMerc * weight}kg weegt.");
            Console.WriteLine($"Op Venus voel je je alsof je {zwVenu * weight}kg weegt.");
            Console.WriteLine($"Op Aarde voel je je alsof je {zwAard * weight}kg weegt.");
            Console.WriteLine($"Op Mars voel je je alsof je {zwMars * weight}kg weegt.");
            Console.WriteLine($"Op Jupiter voel je je alsof je {zwJupi * weight}kg weegt.");
            Console.WriteLine($"Op Saturnus voel je je alsof je {zwSatu * weight}kg weegt.");
            Console.WriteLine($"Op Uranus voel je je alsof je {zwUran * weight}kg weegt.");
            Console.WriteLine($"Op Neptunus voel je je alsof je {zwNept * weight}kg weegt.");
            Console.WriteLine($"Op Pluto voel je je alsof je {zwPlut * weight}kg weegt.");
        }
    }
}
