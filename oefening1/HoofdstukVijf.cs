﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Programmeren
{
    class HoofdstukVijf
    {
        enum Hoofdstuk5 { BMIBerekenaar=1,Schoenverkoper,WetVanOhm,Schrikkeljaar,SimpeleRekenmachine}
        static public void SwitchVijf()
        {
            Console.WriteLine("Welke oefening wilt u openen?");
            Console.WriteLine($"{(int)Hoofdstuk5.BMIBerekenaar}. {Hoofdstuk5.BMIBerekenaar}");
            Console.WriteLine($"{(int)Hoofdstuk5.Schoenverkoper}. {Hoofdstuk5.Schoenverkoper}");
            Console.WriteLine($"{(int)Hoofdstuk5.WetVanOhm}. {Hoofdstuk5.WetVanOhm}");
            Console.WriteLine($"{(int)Hoofdstuk5.Schrikkeljaar}. {Hoofdstuk5.Schrikkeljaar}");
            Console.WriteLine($"{(int)Hoofdstuk5.SimpeleRekenmachine}. {Hoofdstuk5.SimpeleRekenmachine}");
            Hoofdstuk5 Keuze = (Hoofdstuk5)Convert.ToInt32(Console.ReadLine());
            switch (Keuze)
            {
                case Hoofdstuk5.BMIBerekenaar:
                    Console.Clear();
                    BMIBerekenaar();
                    break;
                case Hoofdstuk5.Schoenverkoper:
                    Console.Clear();
                    Schoenverkoper();
                    break;
                case Hoofdstuk5.WetVanOhm:
                    Console.Clear();
                    WetVanOhm();
                    break;
                case Hoofdstuk5.Schrikkeljaar:
                    Console.Clear();
                    Schrikkeljaar();
                    break;
                case Hoofdstuk5.SimpeleRekenmachine:
                    Console.Clear();
                    SimpeleRekenmachine();
                    break;
            }
        }
        static public void BMIBerekenaar()
        {
            Console.WriteLine("Hoeveel weeg je in kg?");
            double weight = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Hoe groot ben je in m?");
            double height = Convert.ToDouble(Console.ReadLine());
            double heightforumule = Math.Pow(height, 2);
            double BMI = weight / heightforumule;
            Console.WriteLine($"Je BMI bedraagt {BMI:F2}.");
            if (BMI < 17.5){
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Ondergewicht");
                Console.ResetColor();
            }
            else if (BMI>=18.5 && BMI<25){
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Normaal Gewicht");
                Console.ResetColor();
            }
            else if (BMI>=25 && BMI<30) {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Overgewicht");
                Console.ResetColor();
            }
            else if (BMI>=30 && BMI<40){
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Zwaarlijvig");
                Console.ResetColor();
            }
            else if (BMI >= 40){
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine("Ernstige obesitas");
                Console.ResetColor();
            }
        }
        static public void Schoenverkoper()
        {
            int price = 20;
            Console.WriteLine("Vanaf hoeveel schoenen geldt de korting?");
            int AmountBeforeDiscount = Convert.ToInt32(Console.ReadLine());
            int PriceDiscount = 10;
            Console.WriteLine("Hoeveel paar schoenen wil je kopen?");
            int amount = Convert.ToInt32(Console.ReadLine());
            if (amount < AmountBeforeDiscount){
                Console.WriteLine($"Je moet {amount * price} euro betalen");
            }
            else if (amount >= AmountBeforeDiscount){
                Console.WriteLine($"Je moet {amount * PriceDiscount} euro betalen");
            }
        }
        static public void WetVanOhm()
        {
            Console.WriteLine("Wat wil je berekenen? Spanning, weerstand of stroomsterkte?");
            string choice = Console.ReadLine();
            if(choice == "spanning")
            {
                Console.WriteLine("Wat is de weerstand?");
                int resistance = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Wat is de stroomsterkte?");
                int current = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine($"De spanning is {resistance * current}");
            }
            else if(choice == "weerstand")
            {
                Console.WriteLine("Wat is de spanning?");
                int voltage = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Wat is de stroomsterkte?");
                int current = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine($"De weerstand is {(voltage/current):f2}");
            }
            else if (choice == "stroomsterkte")
            {
                Console.WriteLine("Wat is de spanning?");
                int voltage = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Wat is de weerstand?");
                int resistance = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine($"De stroomsterkte is {(voltage / resistance):f2}");
            }
        }
        static public void Schrikkeljaar()
        {
            Console.WriteLine("Geef een jaar in");
            int year = Convert.ToInt32(Console.ReadLine());
            if ((year%4) == 0)
            {
                if ((year % 100) == 0)
                {
                    if((year%400) == 0)
                    {
                        Console.WriteLine("Schrikkeljaar");
                    }
                    else
                    {
                        Console.WriteLine("Geen schrikkeljaar");
                    }
                }
                else
                {
                    Console.WriteLine("Schrikkeljaar");
                }
            }
            else
            {
                Console.WriteLine("Geen schrikkeljaar");
            }
        }
        static public void SimpeleRekenmachine()
        {
            Console.WriteLine("Geef je eerste getal in");
            int number1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Geef je tweede getal in");
            int number2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Welke bewerking wil je doen? (+,-,*,/)");
            string operatorFormula = Console.ReadLine();
            if(operatorFormula == "+")
            {
                int solution = number1 + number2;
                Console.WriteLine($"De uitkomst is: {solution}");
            }
            else if(operatorFormula == "-")
            {
                int solution = number1 - number2;
                Console.WriteLine($"De uitkomst is: {solution}");
            }
            else if(operatorFormula == "*")
            {
                int solution = number1 * number2;
                Console.WriteLine($"De uitkomst is: {solution}");
            }
            else if(operatorFormula == "/")
            {
                int solution = number1 / number2;
                Console.WriteLine($"De uitkomst is: {solution}");
            }
        }
    }
}
 