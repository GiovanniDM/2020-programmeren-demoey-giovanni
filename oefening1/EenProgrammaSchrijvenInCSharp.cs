﻿using System;

namespace Programmeren
{
    public class EenProgrammaSchrijvenInCSharp
    {
        enum Hoofdstuk1{MijnEersteProgramma=1, Rommelzin, GekleurdeRommelzin}

        static public void switchEen()
        {

            Console.WriteLine("Welke oefening wilt u openen?");
            Console.WriteLine($"{(int)Hoofdstuk1.MijnEersteProgramma}. {Hoofdstuk1.MijnEersteProgramma}");
            Console.WriteLine($"{(int)Hoofdstuk1.Rommelzin}. {Hoofdstuk1.Rommelzin}");
            Console.WriteLine($"{(int)Hoofdstuk1.GekleurdeRommelzin}. {Hoofdstuk1.GekleurdeRommelzin}");
            Hoofdstuk1 Keuze = (Hoofdstuk1)Convert.ToInt32(Console.ReadLine());
            switch (Keuze)
            {
                case Hoofdstuk1.MijnEersteProgramma:
                    Console.Clear();
                    MijnEersteProgramma();
                    break;
                case Hoofdstuk1.Rommelzin:
                    Console.Clear();
                    Rommelzin();
                    break;
                case Hoofdstuk1.GekleurdeRommelzin:
                    Console.Clear();
                    GekleurdeRommelzin();
                    break;
            }
        }

        static public void MijnEersteProgramma()
        {
            Console.WriteLine("Dis is mijn eerste C#-programma");
            Console.WriteLine("==================================");
            Console.WriteLine("Typ je voornaam:");
            string preName = Console.ReadLine();
            Console.WriteLine("Typ je achternaam:");
            string name = Console.ReadLine();
            Console.WriteLine("");
            Console.WriteLine($"Dus je naam is: {name} {preName}");
            Console.WriteLine($"Of: {preName} {name}");
        }

        static public void Rommelzin()
        {
            Console.WriteLine("Wat is je favoriete kleur?");
            string color = Console.ReadLine();
            Console.WriteLine("Wat is je favoriete eten?");
            string food = Console.ReadLine();
            Console.WriteLine("Wat is je favoriete auto?");
            string car = Console.ReadLine();
            Console.WriteLine("Wat is je favoriete film?");
            string movie = Console.ReadLine();
            Console.WriteLine("Wat is je favoriete boek?");
            string book = Console.ReadLine();
            Console.WriteLine($"Je favoriete kleur is {book}. Je eet graag {color}. Je favoriete auto is {movie}. Je favoriete film is {car}. Je favoriete boek is {food}.");
        }
        static public void GekleurdeRommelzin()
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Wat is je favoriete kleur?");
            Console.ResetColor();
            string color = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("Wat is je favoriete eten?");
            Console.ResetColor();
            string food = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("Wat is je favoriete auto?");
            Console.ResetColor();
            string car = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Wat is je favoriete film?");
            Console.ResetColor();
            string movie = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Wat is je favoriete boek?");
            Console.ResetColor();
            string book = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"Je favoriete kleur is {book}. Je eet graag {color}. Je favoriete auto is {movie}. Je favoriete film is {car}. Je favoriete boek is {food}.");
        }
    }
}
