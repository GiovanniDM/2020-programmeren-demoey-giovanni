﻿using System;

namespace Programmeren
{
    enum hoofdstukken {Hoofdstuk1=1, Hoofdstuk2, Hoofdstuk3, Hoofdstuk4, APDotCom, Hoofdstuk5}

    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welke hoofdstuk wilt u openen?");
            Console.WriteLine($"{(int) hoofdstukken.Hoofdstuk1}. {hoofdstukken.Hoofdstuk1}");
            Console.WriteLine($"{(int)hoofdstukken.Hoofdstuk2}. {hoofdstukken.Hoofdstuk2}");
            Console.WriteLine($"{(int)hoofdstukken.Hoofdstuk3}. {hoofdstukken.Hoofdstuk3}");
            Console.WriteLine($"{(int)hoofdstukken.Hoofdstuk4}. {hoofdstukken.Hoofdstuk4}");
            Console.WriteLine($"{(int)hoofdstukken.APDotCom}. {hoofdstukken.APDotCom}");
            Console.WriteLine($"{(int)hoofdstukken.Hoofdstuk5}. {hoofdstukken.Hoofdstuk5}");
            hoofdstukken HoofdstukKeuze = (hoofdstukken)Convert.ToInt32(Console.ReadLine());
            switch (HoofdstukKeuze)
            {
                case hoofdstukken.Hoofdstuk1:
                    Console.Clear();
                    EenProgrammaSchrijvenInCSharp.switchEen();
                    break;
                case hoofdstukken.Hoofdstuk2:
                    Console.Clear();
                    HoofdStukTwee.switchTwee();
                    break;
                case hoofdstukken.Hoofdstuk3:
                    Console.Clear();
                    HoofdstukDrie.switchDrie();
                    break;
                case hoofdstukken.Hoofdstuk4:
                    Console.Clear();
                    HoofdstukVier.switchVier();
                    break;
                case hoofdstukken.APDotCom:
                    Console.Clear();
                    APDotCom.switchAPDotCom();
                    break;
                case hoofdstukken.Hoofdstuk5:
                    Console.Clear();
                    HoofdstukVijf.SwitchVijf();
                    break;
            }
        }
    }
}
