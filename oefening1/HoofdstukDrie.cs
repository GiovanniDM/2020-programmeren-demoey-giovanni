﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Programmeren
{
    public class HoofdstukDrie
    {
        enum Hoofdstuk3 {VariabelenHoofdletters=1, StringInterpolation, BerekenBtw, LeetSpeak, Instructies, Lotto, SuperLotto}

        static public void switchDrie()
        {

            Console.WriteLine("Welke oefening wilt u openen?");
            Console.WriteLine($"{(int)Hoofdstuk3.VariabelenHoofdletters}. {Hoofdstuk3.VariabelenHoofdletters}");
            Console.WriteLine($"{(int)Hoofdstuk3.StringInterpolation}. {Hoofdstuk3.StringInterpolation}");
            Console.WriteLine($"{(int)Hoofdstuk3.BerekenBtw}. {Hoofdstuk3.BerekenBtw}");
            Console.WriteLine($"{(int)Hoofdstuk3.LeetSpeak}. {Hoofdstuk3.LeetSpeak}");
            Console.WriteLine($"{(int)Hoofdstuk3.Instructies}. {Hoofdstuk3.Instructies}");
            Console.WriteLine($"{(int)Hoofdstuk3.Lotto}. {Hoofdstuk3.Lotto}");
            Console.WriteLine($"{(int)Hoofdstuk3.SuperLotto}. {Hoofdstuk3.SuperLotto}");
            Hoofdstuk3 Keuze = (Hoofdstuk3)Convert.ToInt32(Console.ReadLine());
            switch (Keuze)
            {
                case Hoofdstuk3.VariabelenHoofdletters:
                    Console.Clear();
                    VariabelenHoofdletters();
                    break;
                case Hoofdstuk3.StringInterpolation:
                    Console.Clear();
                    StringInterpolation();
                    break;
                case Hoofdstuk3.BerekenBtw:
                    Console.Clear();
                    BerekenBtw();
                    break;
                case Hoofdstuk3.LeetSpeak:
                    Console.Clear();
                    LeetSpeak();
                    break;
                case Hoofdstuk3.Instructies:
                    Console.Clear();
                    Instructies();
                    break;
                case Hoofdstuk3.Lotto:
                    Console.Clear();
                    Lotto();
                    break;
                case Hoofdstuk3.SuperLotto:
                    Console.Clear();
                    SuperLotto();
                    break;
            }
        }
        static public void VariabelenHoofdletters()
        {
            Console.WriteLine("Welke tekst moet ik omzetten?");
            string text = Console.ReadLine();
            Console.WriteLine(text.ToUpper());
        }

        static public void StringInterpolation()
        {
            Console.WriteLine("String Interpolation is al gebruikt in mijn oefeningen H2-Maaltafels en H2-Ruimte.");
        }

        static public void BerekenBtw()
        {
            Console.WriteLine("Geef het bedrag in:");
            double price = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Geef het BTW percentage in:");
            double btwPercentage = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Het bedrag {price} met {btwPercentage}% btw bedraagt {(price+((price/100)*btwPercentage)):F2}");
        }

        static public void LeetSpeak()
        {
            Console.WriteLine("Geef je tekst in");
            string text = Console.ReadLine();
            string text2 = text.Replace("a", "@");
            string text3 = text2.Replace(" ", "");
            Console.WriteLine(text3);
        }

        static public void Instructies()
        {
            Console.WriteLine("Wat is je naam?");
            string name = Console.ReadLine();
            string name2 = name.ToUpper();
            Console.WriteLine("Wat is de naam van de cursus?");
            string course = Console.ReadLine();
            Console.WriteLine($"Maak een map als volgt: \\{name2.Substring(0,3)}\\{course}");
        }

        static public void Lotto()
        {
            Console.WriteLine("Wat zijn je cijfers (tussen 01 en 45)?");
            string numbers = Console.ReadLine();
            string numbers1 = numbers.Substring(0, 8);
            string numbers2 = numbers.Substring(9);
            Console.WriteLine($"Je cijfers zijn: \n{numbers1.Replace(",", "\t")}\n{numbers2.Replace(",", "\t")}");
        }

        static public void SuperLotto()
        {
            Console.WriteLine("Wat zijn je cijfers (tussen 01 en 45)?");
            string numbers = Console.ReadLine();

            /* Eerste lottogetal: zoek de eerste komma, 
             * knip de string tot aan de index van die komma,
             * converteer het eerste getal naar een integer, zodat we 
             * in de stringinterpolatie een formatstring met voorloopnullen
             * kunnen gebruiken */
            int indexComma = numbers.IndexOf(",");
            int lottoNumber = Convert.ToInt32(numbers.Substring(0, indexComma));
            Console.Write($"{lottoNumber:D2}\t");

            //Tweede lottogetal
            numbers = numbers.Substring(indexComma + 1); //resterende string, na de eerste komma
            indexComma = numbers.IndexOf(","); //index van de eerste komma in de resterende string (dus de tweede komma in de vorige, volledige string)
            lottoNumber = Convert.ToInt32(numbers.Substring(0, indexComma));
            Console.Write($"{lottoNumber:D2}\t");

            //Getal 3 (en regeleinde)
            numbers = numbers.Substring(indexComma + 1); //resterende string, na de eerstvolgende komma
            indexComma = numbers.IndexOf(",");
            lottoNumber = Convert.ToInt32(numbers.Substring(0, indexComma));
            Console.WriteLine($"{lottoNumber:D2}");

            //Getal 4
            numbers = numbers.Substring(indexComma + 1);
            indexComma = numbers.IndexOf(",");
            lottoNumber = Convert.ToInt32(numbers.Substring(0, indexComma));
            Console.Write($"{lottoNumber:D2}\t");

            //Getal 5
            numbers = numbers.Substring(indexComma + 1);
            indexComma = numbers.IndexOf(",");
            lottoNumber = Convert.ToInt32(numbers.Substring(0, indexComma));
            Console.Write($"{lottoNumber:D2}\t");

            //Getal 6: zoeken naar komma niet meer nodig
            numbers = numbers.Substring(indexComma + 1);
            lottoNumber = Convert.ToInt32(numbers);
            Console.WriteLine($"{lottoNumber:D2}");
        }
    }
}
